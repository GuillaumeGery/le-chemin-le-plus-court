<?php

    class ShortestPathAlgo
    {
        private $shortestPath = [];

        public function getSp()
        {
            return $this->shortestPath;
        }

        function getShortestPath($map, $currentPoint, $end, $currentPath)
        {

            $currentPath[] = $currentPoint;

            if (!empty($this->shortestPath) && count($currentPath) >= count($this->shortestPath)) {
                return;
            }

            if ($currentPoint[0] == $end[0] && $currentPoint[1] == $end[1]) {
                return $this->shortestPath = $currentPath;
                
            }

            $points = [
                [$currentPoint[0] - 1, $currentPoint[1]],
                [$currentPoint[0] + 1, $currentPoint[1]],
                [$currentPoint[0], $currentPoint[1] - 1],
                [$currentPoint[0], $currentPoint[1] + 1],
            ];

            foreach ($points as $point) {
                if ($point[0] < 0 || $point[1] < 0 || $point[0] >= count($map) || $point[1] >= count($map[0])) {
                    continue;
                }
                if (0 == $map[$point[0]][$point[1]]) {
                    continue;
                }
                if (in_array($point, $currentPath)) {
                    continue;
                }
                
                $this->getShortestPath($map, $point, $end, $currentPath);
            }
        }
    }
