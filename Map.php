<?php
require_once "ShortestPathAlgo.php";
class Map 
{
    private $columns;
    private $rows;
    private $map = [];

    public function __construct($columns, $rows, ShortestPathAlgo $algo)
    {
        $this->columns = $columns;
        $this->rows = $rows;
        $this->algo = $algo;
    }
    
    public function getMap()
    {
        for ($j=0; $j<$this->rows; $j++) {
            $array = [];
            for ($i=0; $i<$this->columns; $i++) {
                $n = random_int(0,1);
                $array[] = $n;
            }
            $this->map[] = $array;
        }
        return $this->map;
    }
    public function drewMap()
    {
       
        foreach ($this->map as $r => $row) {
            echo '|';
            foreach ($row as $c => $cell) {
                    echo ($cell ? '_' : 'x');
                    echo '|';
            }
            echo "<br>";
        }
    }

    public function drewShortestPath() 
    {
        foreach ($this->map as $r => $row) {
            echo '|';
            foreach ($row as $c => $cell) {
                if (($pos = array_search([$r, $c], $this->algo->getSp())) !== false) {
                    switch ($pos) {
                        case 0:
                            echo 'D';
                            break;
                        case count($this->algo->getSp())-1:
                            echo 'A';
                            break;
                        default:
                            echo 'o';
                            break;
                    }
                } else {
                    echo ($cell ? '_' : 'x');
                }
                echo '|';
            }
            echo "<br>";
        }
    }

}