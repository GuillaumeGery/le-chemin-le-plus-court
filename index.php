<?php
require_once "ShortestPathAlgo.php";
require_once "Map.php";

    $columns = $_POST['columns'];
    $rows = $_POST['rows'];

    $rowS = (int)$_POST['rowS'];
    $colS = (int)$_POST['colS'];
    $rowE = (int)$_POST['rowE'];
    $colE = (int)$_POST['colE'];    


    $start[] = $rowS;
    $start[] = $colS;
    $end[] = $rowE;
    $end[] = $colE;
    
    $sh = new ShortestPathAlgo();
    $m = new Map($columns, $rows, $sh);
    

    $map[] = $m->getMap();
    $sh->getShortestPath($map, $start, $end, []);
    $shortestPath = $sh->getSp();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Shortest Path</title>
</head>
<body>

    <div class="card text-center">
    <div class="card-header">
        <h5>The shortest path</h5>
    </div>
    <div class="card-body">
            <form method="POST">
                <div class="row">
                    <div class="col">
                    <input type="number" name="rows" class="form-control" value="<?php  echo htmlentities($_POST['rows']);?>" placeholder="Nombre de lignes">
                    </div>
                    <div class="col">
                    <button type="submit" name="gen" class="btn btn-primary">Générer votre map</button>
                    </div>
                    <div class="col">
                    <input type="number" name="columns" class="form-control" value="<?php if(isset($_POST['columns'])) { echo htmlentities($_POST['columns']);}?>" placeholder="Nombre de colonnes">
                    </div>
                </div>

        <p class="card-text">
            <?php
            if (isset($_POST['gen']) && !empty($_POST['columns'] && !empty($_POST['rows']))) {
                $m->drewMap();
            }
            else 
            {
                echo "Veuillez saisir le nombre de colonnes et de lignes";
            }
            ?>
        
                <div class="row">
                    <div class="col">
                    <input type="number" name = "rowS" class="form-control" placeholder="Ligne du point de départ">
                    </div>
                    <div class="col">
                    <input type="number" name = "colS" class="form-control" placeholder="Colonne du point de départ">
                    </div>
                    <div class="col">
                    <button type="submit" name = "find" class="btn btn-primary">Trouver le chemin le plus court</button>
                    </div>
                    <div class="col">
                    <input type="number" name = "rowE" class="form-control" placeholder="Ligne du point d'arrivage">
                    </div>
                    <div class="col">
                    <input type="number" name = "colE" class="form-control" placeholder="Colonne du point d'arrivage">
                    </div>
                </div>
            </form>
    </div>
    <?php
        // if (!empty($_POST['colS']) && !empty($_POST['rowS']) && !empty($_POST['colE']) && !empty($_POST['rowE'])) {
            $m->drewShortestPath();
        // }
        // else 
        // {
        //     echo "Veuillez saisir le points de départ et le point d'arriver";
        // }
    ?>
    <div class="card-footer text-muted">
        
        <?php 
        if (count($shortestPath)> 0) {
            echo 'shortest path: ' . count($shortestPath) - 1 . ' steps';
        }
        else echo "There is no shortest path";
        ?>
    </div>
    </div>
</body>
</html>
