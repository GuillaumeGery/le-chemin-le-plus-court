# Approche initial du projet le chemin le plus court 
Rany et Guillaume 

## Problématique 
- L'objectif est de calculer et d'afficher le plus court chemin entre 2 points, à partir des données fournies en entrée :

    une map, représentée par un tableau à 2 dimensions ;
    les coordonnées du point de départ ;
    les coordonnées du point d'arrivée.

​
- On doit obtenir en sortie :

    la longueur du chemin le plus court ;
    la représentation de la map avec ce chemin.

## Liste des Tâches 

- Créer un dépôt Git , avec une branche main , une branche de développement pour Rany et une branche de développement pour Guillaume 
  
- Installation de Docker 
- Définir l’environnement de travail dans Docker ( Images ) 
- Créer le fichier docker-compose 
- Analyser le fichier PHP Version procédurale 
- Réfléchir à la conception ( UML ) 
- Suite à l’analyse , transformer le code en POO 

